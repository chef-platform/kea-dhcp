Changelog
=========

1.2.0
-----

Main:

- fix: set name attribute to memfile
  + Workaround to replace wrong lease file path given by default
    in kea-1.6.0-4.el7.x86_64.rpm built for CentOS  
    See https://gitlab.isc.org/isc-projects/kea/issues/1144
- feat: update default config to match kea-1.6.0
- feat: move logging entry under daemon scope
  + ref: https://gitlab.isc.org/isc-projects/kea/issues/208
- feat: separate config file for each service
  + ref: https://oldkea.isc.org/ticket/533
- feat: do not set exporter on dhcp server
- feat: use yum-epel cookbook for epel

Tests:

- test: use google dns to fix internal dns in dind
- test: make kitchen.yml config file visible
- test: switch to chefplatform image
- test: include .gitlab-ci.yml from test-cookbook
- test: replace deprecated require_chef_omnibus
- fix: add rspec-core to Gemfile
- fix: accept chef license

Misc:
- chore: set generic maintainer & helpdesk email
- chore: add supermarket category in .category
- doc: use doc in git message instead of docs
- chore: set 2020 in copyright notice
- style(rubocop): fix multiple offenses

1.1.0
-----

Main:

- feat: add prometheus exporter to check ip validity

Tests:

- use .gitlab-ci.yml template [20170731]
- add a linkdelay to improve test resilience

Misc:

- docs: use karma for git format in contributing
- style(rubocop): fix heredoc delimiters

1.0.0
-----

Main:

- Initial version with Centos 7 support importing code from dhcp-wrapper
- Add the installation and configuration of Kea, a modern DHCP server from ISC
